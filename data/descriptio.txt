
Features	Description
cli_pl_header	http client response header size
cli_pl_body	http client response payload size
cli_cont_len	http client declared content length (in the header field)
srv_pl_header	http server response header size
srv_pl_body	http server response payload size
srv_cont_len	http server declared content length (in the header field)
aggregated_sessions	number of requests aggregated into one entry
bytes	Number of bytes transmitted from the clientandserver comprising the TCP stack header
net_samples	— used internally
tcp_frag	Number of fragmented packets
tcp_pkts	Number of server transmitted packets
tcp_retr	Number of retransmitted packets
tcp_ooo	Number of out of order packets
cli_tcp_pkts	Number of server transmitted packets (Client)
cli_tcp_ooo	Number of out of order packets (Client)
cli_tcp_retr	Number of retransmitted packets (Client)
cli_tcp_frag	Number of fragmented packets (Client)
cli_tcp_empty	How many empty TCP packets have been transmitted (Client)
cli_win_change	How many times theclient receive window has beenchanged
cli_win_zero	How many times the client receive window has been closed
cli_tcp_full	How many packets with full payload have been transmitted (Client)
cli_tcp_tot_bytes	Client TCP total bytes
cli_pl_tot	Client total payload
cli_pl_change	How many times the payload has been changed (Client)
srv_tcp_pkts	Number of server transmitted packets (Server)
srv_tcp_ooo	Number of out of order packets (Server)
srv_tcp_retr	Number of retransmitted packets (Server)
srv_tcp_frag	Number of fragmented packets (Server)
srv_tcp_empty	How many empty TCP packets have been transmitted (Server)
srv_win_change	How many times the server receive window has been changed
srv_win_zero	How many times the server receive window has been closed
srv_tcp_full	How many packets with full payload have been transmitted (Server)
srv_tcp_tot_bytes	Server TCP total bytes
srv_pl_tot	Server total payload
srv_pl_change	How many times the payload has been changed (Server)
srv_tcp_win	Last server tcp receive window size
srv_tx_time	Server data transmission time
cli_tcp_win	Last client tcp receive window size
client_latency	Estimated packet delay between client and probe
application_latency	Calculated application response time
cli_tx_time	Client data transmission time
load_time	Roundtrip time since the client request starts up to all server response data are received from client: ~= application_latency+cli_tx_time+srv_tx_time
server_latency	Estimated packet delay between server and probe
proxy	Flag to identify if it has been used a proxy
sp_healthscore	The healthscore specifies a value between 0 and 10, where 0 represents a low load and a high ability to process requests and 10 represents a high load and that the server is throttling requests to maintain adequate throughput
sp_req_duration	Time elapsed to elaborate the response by the server
sp_is_lat	IS latency
sp_error	If the protocol server rejects the request because the current processing load on the server exceeds its capacity, the protocol server includes a SharePointError header set to 2 in the response. If the protocol server renders an error page to the client for any other reason, the protocol server includes a SharePointError header set to zero in the response
throughput	Bytes/load_time
