Priečinok 'data':
- tu sa nachádzajú jednotlivé použité dátové súbory - trénovacia sada a testovacia sada, pričom tieto dve majú aj k sebe prislúchajúci súbor obsahujúci triedu pre jednotlivé pozorovania
- ďalším súborom je 'descriptio.txt' ktorý obsahuje popis jendotlivých atribútov (tak ako je na stránke súťaže)
- 'header.csv' obsahuje hlavičku súborov s názvami jednotlivých atribútov
- navyše tento priečinok obsahuje súbory 'my_train.csv' a 'my_valid.csv', ktoré predstavujú mnou rozdelenú trénovaciu sadu na trénovacie dáta a validačné dáta (za účelom zreprodukovania výsledkov)

Priečinok 'eval':
- obsahuje python script používaný na vyhodnotenie úspešnosti modelu (s mnou pridanou metódou aby sa nemuselo vždy cez súbory ísť)

Priečinok 'models':
- obsahuje jednotlivé natrénované modely aby ich nebolo nutné vždy nanovo trénovať (lebo občas to môže trvať dlhšie) ale stačilo ich iba načítať.

Priečinok 'results':
- obsahuje výsledky jednotlivých nastavení parametrov - 'tree.txt' pre nastavenie pre rozhodovacie stromy, 'forest.txt' pre nastavenie pre náhodné lesy a 'bagging.txt' pre nastavenie pre bagging stromčekov.


Súbor 'Project.ipynb' obsahuje vypracovanie projektu aj s mojimi komentármi - je to Jupyter notebook takže na jeho spustenie je potrebné mať knižnicu Jupyter-notebook.

Súbor main.py obsahuje python skript využívaný na nastavenie hyperparametrov a následné testovanie jednotlivých modelov. Taktiež obsahuje otestovanie najelpších modelov na testovacích dátach.

Súbor 'ML_projekt.pdf' obsahuje odovzdaný report pre tento projekt.

Súbor 'ML_napad.pdf' obsahuje popis nápadu odovzdaný na predmet za účelom jeho vyhodnotenia a prípadnej úpravy.


