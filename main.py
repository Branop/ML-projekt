import numpy as np
import random
import math
from sklearn.ensemble import RandomForestClassifier as Forest
from sklearn.tree import DecisionTreeClassifier as Tree
from sklearn.ensemble import BaggingClassifier
import pickle
import eval.eval as e
import matplotlib as plt
import seaborn
import pandas as pd
pd.set_option('display.max_columns', 50)


def normalize(data, mean=None, std=None):
    if mean is None or std is None:
        mean = np.mean(data, axis=0, keepdims=True)
        std = np.mean(data, axis=0, keepdims=True)
    return (data - mean) / std, mean, std

SPLIT = False
RECALCULATE_MODELS = False

names = pd.read_csv('data/header.csv', sep='\t')
train_data = pd.read_csv('data/my_train.csv', header=None, names=names, index_col=False)
name_list = ['cli_pl_body', 'cli_cont_len', 'aggregated_sessions', 'net_samples', 'tcp_frag', 'tcp_ooo',
             'cli_tcp_ooo', 'cli_tcp_retr', 'cli_win_change', 'cli_tcp_frag', 'cli_win_zero', 'cli_tcp_full', 'cli_pl_change', 'srv_tcp_ooo',
             'srv_tcp_frag', 'srv_win_zero', 'cli_tx_time', 'proxy', 'sp_healthscore', 'sp_req_duration',
             'sp_is_lat', 'sp_error']
indices = [list(names).index(word) for word in name_list]
idx = np.ones(names.shape[1]).astype(bool)
idx[indices] = False
t_data = train_data.as_matrix()[:,idx]
t_data_y = t_data[:,-1].astype(int)
t_data = t_data[:,:-1]
t_data, mean, std = normalize(t_data)
validation_data = np.genfromtxt('data/my_valid.csv', delimiter=',')
validation_data = validation_data[:,idx]
validation_y = validation_data[:,-1].astype(int)
validation_data = validation_data[:,:-1]
validation_data, mean, std = normalize(validation_data, mean, std)
print('Data ready')


def forest_grid_search():
    with open('results/forest.txt', 'w') as file:
        print("Forest:", file=file)
        print('(n_estimators)', file=file)
        print('', file=file)
        best_params = None
        best_macro = -1
        best_micro = -1
        forest_dict = {'n_estimators': [10, 20, 25, 30, 45, 50, 75, 90, 100, 125, 150, 175, 200]}
        for est in forest_dict['n_estimators']:
            print(est)
            print(est, file=file)
            forest = Forest(random_state=42, criterion='entropy', max_depth=None, max_features=0.75, class_weight='balanced', n_estimators=est)
            forest.fit(t_data, t_data_y)
            out = forest.predict(validation_data).astype(int)
            macro, micro = e.return_results(out.astype(str), validation_y.astype(str), predicted_as_file=False, validation_in_file=False)
            print((macro, micro), file=file)
            print('', file=file)
            if best_macro <= macro:
                best_params = (est)
                best_macro = macro
                best_micro = micro
        print(best_params, file=file)
        print(best_macro, file=file)
        print(best_micro, file=file)


def tree_grid_search():
    with open('results/tree.txt', 'w') as file:
        print("Trees:", file=file)
        print('(criterion, max_depth, max_features, class_weight)', file=file)
        print('', file=file)
        best_params = None
        best_macro = -1
        best_micro = -1
        tree_dict = {'criterion': ['gini', 'entropy'], 'max_depth': [5, 10, 15, 20, 25, None], 'max_features': [0.25, 0.5, .75, 'sqrt', 'log2', None], 'class_weight': ['balanced', None]}
        for crit in tree_dict['criterion']:
            for depth in tree_dict['max_depth']:
                for features in tree_dict['max_features']:
                    for weight in tree_dict['class_weight']:
                        print((crit, depth, features, weight))
                        print((crit, depth, features, weight), file=file)
                        tree = Tree(random_state=42, criterion=crit, max_depth=depth, max_features=features, class_weight=weight)
                        tree.fit(t_data, t_data_y)
                        out = tree.predict(validation_data).astype(int)
                        macro, micro = e.return_results(out.astype(str), validation_y.astype(str), predicted_as_file=False, validation_in_file=False)
                        print((macro, micro), file=file)
                        print('', file=file)
                        if best_macro <= macro:
                            best_params = (crit, depth, features, weight)
                            best_macro = macro
                            best_micro = micro
        print(best_params, file=file)
        print(best_macro, file=file)
        print(best_micro, file=file)


def bagging_grid_search():
    with open('results/bagging.txt', 'w') as file:
        print("Bagging:", file=file)
        print('(n_estimators, max_samples, max_features, bootstrap_samples)', file=file)
        print('', file=file)
        best_params = None
        best_macro = -1
        best_micro = -1
        bagging_dict = {'n_estimators': [10, 25, 50, 75, 100, 150, 175], 'max_samples': [0.25, 0.5, 0.75, 1.0], 'max_features': [0.25, 0.5, 0.75, 1.0], 'bootstrap_features': [False, True]}
        for est in bagging_dict['n_estimators']:
            for samples in bagging_dict['max_samples']:
                for feature in bagging_dict['max_features']:
                    for bootstrap in bagging_dict['bootstrap_features']:
                        print((est, samples, feature, bootstrap))
                        print((est, samples, feature, bootstrap), file=file)
                        bag = BaggingClassifier(random_state=42, base_estimator=Tree(criterion='entropy', max_depth=None, max_features=0.75, class_weight='balanced', random_state=42),
                                                n_estimators=est, max_samples=samples, max_features=feature, bootstrap_features=bootstrap)
                        bag.fit(t_data, t_data_y)
                        out = bag.predict(validation_data).astype(int)
                        macro, micro = e.return_results(out.astype(str), validation_y.astype(str), predicted_as_file=False, validation_in_file=False)
                        print((macro, micro), file=file)
                        print('', file=file)
                        if best_macro <= macro:
                            best_params = (est, samples, feature, bootstrap)
                            best_macro = macro
                            best_micro = micro
        print(best_params, file=file)
        print(best_macro, file=file)
        print(best_micro, file=file)

#tree_grid_search()
#forest_grid_search()
#bagging_grid_search()

del train_data
del t_data
del t_data_y
del validation_data
del validation_y

idx = idx[:-1]
data = np.genfromtxt('data/train.csv', delimiter='\t', skip_header=1)
data_y = np.genfromtxt('data/train_target.csv', delimiter='\t').astype(int)
data = data[:, idx]
data, mean, std = normalize(data)
valid_data = np.genfromtxt('data/valid.csv', delimiter='\t', skip_header=1)
valid_y = np.genfromtxt('data/valid_target.csv', delimiter='\t').astype(int)
valid_data = valid_data[:,idx]
valid_data, mean, std = normalize(valid_data, mean, std)


def tree_test():
    if RECALCULATE_MODELS:
        tree = Tree(random_state=42, criterion='entropy', max_depth=None, max_features=0.75, class_weight='balanced')
        tree.fit(data, data_y)
        with open('models/tree.pkl', 'wb') as file:
            pickle.dump(tree, file)
    else:
        with open('models/tree.pkl', 'rb') as file:
            tree = pickle.load(file)
    out = tree.predict(valid_data)
    macro, micro = e.return_results(out.astype(str), valid_y.astype(str), predicted_as_file=False, validation_in_file=False)
    print((macro, micro))


def forest_test():
    if RECALCULATE_MODELS:
        forest = Forest(random_state=42, criterion='entropy', max_depth=None, max_features=0.75, class_weight='balanced', n_estimators=125)
        forest.fit(data, data_y)
        with open('models/forest.pkl', 'wb') as file:
            pickle.dump(forest, file)
    else:
        with open('models/forest.pkl', 'rb') as file:
            forest = pickle.load(file)
    out = forest.predict(valid_data).astype(int)
    macro, micro = e.return_results(out.astype(str), valid_y.astype(str), predicted_as_file=False, validation_in_file=False)
    print((macro, micro))


def bagging_test():
    if RECALCULATE_MODELS:
        bagging = BaggingClassifier(random_state=42, base_estimator=Tree(criterion='entropy', max_depth=None, max_features=0.75, class_weight='balanced', random_state=42),
                                    n_estimators=150, max_samples=1.0, max_features=0.75, bootstrap_features=False)
        bagging.fit(data, data_y)
        with open('models/bagging.pkl', 'wb') as file:
            pickle.dump(bagging, file)
    else:
        with open('models/bagging.pkl', 'rb') as file:
            bagging = pickle.load(file)
    out = bagging.predict(valid_data).astype(int)
    macro, micro = e.return_results(out.astype(str), valid_y.astype(str), predicted_as_file=False, validation_in_file=False)
    print((macro, micro))

tree_test()
forest_test()
bagging_test()

